Ensure that you have Python ^3.8, `Poetry <https://python-poetry.org/>`_ and you have followed the installation procedure.


Performance Gap
===============

With PREDYCE it is possible to run performance gap analyses. This scenario allows to retrieve the same KPIs on simulated and monitored data, computing the delta among them (monitored--simulated).
The preliminary actions field in the JSON file can contain a list of JSONs, each define a different simulation setting.
This allows to compare monitored data with different operational settings (e.g., defined according to different standards).
Monitored data should be provided in input in CSV format: column names should respect variables nomenclature used inside PREDYCE (e.g. *T_db_i[C]*: indoor dry-bulb temperature) and IDF zoning structure in order to match sensors with zones (e.g. *Apartments_1th_T_db_i[C]*).

What you need
-------------
* `EnergyPlus <https://energyplus.net/>`_ installed on your system
* IDF file of the same version of EnergyPlus
* EPW file(s)
* Input JSON file

.. tip::
    IDF version is found on the top of IDF file if you open it with a text editor::

        Version, 8.9.0.001;                               !- Version Identifier
    
    The IDF version must be the same as the one of EnergyPlus installed on the system;
    if not you can install the correct version of EnergyPlus or update the
    IDF file with the built-in IDFVersionUpdater of EnergyPlus under the PreProcess
    folder. For more information visit `IDF Version Updater <https://bigladdersoftware.com/epx/docs/8-9/auxiliary-programs/idf-version-updater.html>`_ page.

.. tip::
    You do not need to specify the IDD file but only its version, e.g. "8.9".

.. tip::
    All files that you need are included as examples in the PREDYCE repository.

YAML configuration file
^^^^^^^^^^^^^^^^^^^^^^^
This file is used to specify some parameters which are usually carried among different experiments and they do not need to be changed very often.

Create you own configuration file by renaming **config.example.yml** into **config.yml**.

Here you can specify different things:

:plot_dir: Directory where KPI plots are saved
:out_dir: Directory where CSV file of results is saved
:num_of_cpus: number of parallel simulations, defaults to 0 (auto, i.e. match CPU cores)
:idd_version: Version of EnergyPlus and IDF, e.g. "8.9"
  
JSON file for performance gap
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This file is used to create a specific pool of simulations by combining different actions and KPI outputs.

JSON input file example::

    {
        "scenario": "performance_gap",
        "building_name": "Apartments",
        "start_date": "2021-01-08",
        "end_date": "2021-12-31",
        "kpi": {
            "Q_h": {
                "variable": "Baseboard Hot Water Energy",
                "ep_factor": 1.05
            },
            "timeseries_Q_h": {
                "variable": "Baseboard Hot Water Energy",
                "ep_factor": 1.05
            },
            "n_co2_bI": {},
            "n_co2_aIII": {},
            "timeseries_t_op": {
                "season": "free_running",
                "filter_by": "Appartments"
            }
        },
        "aggregations": {
            "Q_h": [
                "Apartments",
                "Apartments_1th",
                "Apartments_1tv",
                "Apartments_2th",
                "Apartments_2tv"
            ]
        },
        "preliminary_actions": [
            {
                "change_setpoint": {
                    "heat": 20
                }
            },
            {
                "change_setpoint": {
                    "heat": 21.8
                },
                "change_occupancy_1th": {
                    "value": 0.009,
                    "filter_by": "Apartments_1th",
                    "Number_of_People_Schedule_Name": "1th_occ_adapted",
                    "relative": false
                },
                "change_occupancy_1tv": {
                    "value": 0.009,
                    "filter_by": "Apartments_1tv",
                    "Number_of_People_Schedule_Name": "1tv_occ_adapted",
                    "relative": false
                }
            }
        ]
    }


:building_name: The name of building which specifies zones that are included in the computations.
:preliminary_actions: List of JSONs containing actions which define the different building operational settings and other needed parameters (e.g. output variables of interest).
:kpi: List of KPIs that will be computed on both simulation output and provided monitored data.
:aggregations: List of zones (or aggregations of zones) on which each KPI will be computed separately.
:start_date: Start of the evaluation period, simulation is executed starting from one week before to cut off the warm up period.
:end_date: End of the evaluation period.


Running from command line 
-------------------------
Options
^^^^^^^

-d, --output-directory  output folder
-t, --temp-directory  temp folder
-p, --plot-directory  plot folder
-f, --input-file  input JSON file
-w, --weather  EPW file or EPW directory
-m, --measured-data  data CSV file
-i, --idd  IDD version
-j, --jobs  number of parallel simulations, defaults to 0 (auto)
-o, --original  include original model in dataframe, defaults to ``True``
--checkpoint-data  checkpoint data path
-c, --checkpoint-interval  checkpoint interval, defaults to 128

.. warning::
    Command line options will override YAML configuration file options.

Running
^^^^^^^

From a terminal, activate the virtual environment::

    poetry shell

Run performance gap::

    python run_performance_gap.py "my_model.idf" -f "my_input.json" -m "my_monitored_data.csv" -w "my_epw.epw" -o

Outputs
-------
Output structure is the same than in other PREDYCE scenarios:

* The different simulations (generated by the JSONs in the preliminary actions list) are identified with *simulation_1*, *simulation_2*, and so on; monitored data are identified with *monitored*; computed gaps are identified with *delta_1*, *delta_2*, and so on.
* In the **output** directory you get a **data_res.csv** file with the list of all executed simulations and the respective results.
  Particularly, there will be a line for each simulation (identified with index *simulation_n*), a line for results computed on monitored data (identified with index *monitored*), and a line for each gap result depending on the number of simulation settings (identified with index *delta_n*).
* In the **plot** directory you get a list of numbered subfolders which follow the indexing of **data_res.csv**. Inside each folder you can find the plots for the corresponding simulation;
  moreover, the **data_res_timeseries.csv** file contains the results of timeseries KPIs: indexes identify timestamps while column names contain reference index (*simulated*, *monitored* or *delta*), zoning aggregation, and KPI name (e.g. *delta_1_Apartments_1th_timeseries_Q_h[W/m2]*).
