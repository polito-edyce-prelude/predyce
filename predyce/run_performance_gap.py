from predyce.IDF_class import IDF
from predyce.idf_editor import set_fields
from predyce.runner import Runner, idf_editor
from pathlib import Path
import pandas as pd
import numpy as np
import json
import yaml
from runner import NpEncoder
import argparse
import tempfile
import datetime
from predyce.kpi import KPI
from predyce.epw_reader import retrieve_column
from datetime import datetime, timedelta
import os
import uuid
import copy
import re

# Absolute path of the directory of current script.
P = Path(__file__).parent.absolute()
# Configuration file.
with open(P / "config.yml") as config_file:
    CONFIG = yaml.load(config_file, Loader=yaml.FullLoader)

# Argument parameters which override the ones in configuration file.
parser = argparse.ArgumentParser()
parser.add_argument("--checkpoint-data", help="Checkpoint data absolute path (default: None)", default=None)
parser.add_argument("-c", "--checkpoint-interval", help="Checkpoint interval (default: 128)", default=128, type=int)
parser.add_argument("-d", "--output-directory", help="Output directory absolute path (default: /predyce-out)", default=CONFIG["out_dir"])
parser.add_argument("-i", "--idd", help='IDD version', default=CONFIG["idd_version"])
parser.add_argument("-f", "--input-file", help="Input data file absolute path (default: in.json in executable directory" , default=P / "in.json")
# parser.add_argument("-f", "--input-file", help="List of input data files (default: in.json in executable directory" , default=P / "in.json", nargs="*")
parser.add_argument("-j", "--jobs", help= "Multi-process with N processes (0=auto)", default=CONFIG["num_of_cpus"], type=int)
parser.add_argument("-o", "--original", help="Include original model in dataframe permutations (default: True)", action="store_false", default=True)
parser.add_argument("-m", "--measured-data", help="Measured environmental indoor data file absolute path (default: None)", default="")
parser.add_argument("-p", "--plot-directory", help="Plot directory path (default: /predyce-plots)", default=CONFIG["plot_dir"])
parser.add_argument("-t", "--temp-directory", help="Temp directory path (default: System temp folder)", default=CONFIG["temp_dir"])
parser.add_argument("-w", "--weather", help= "Weather data absolute path in CSV or EPW format", default=CONFIG["epw"])
parser.add_argument("model", help= "Calibrated building model path (IDF format only accepted)")

args = parser.parse_args()

# IDF file.
fname = args.model

# Find and set IDD file.
idd_folder = Path(__file__).parent.absolute() / "resources" / "iddfiles"
idd_version = str(args.idd).replace(".", "-")
for entry in idd_folder.iterdir():
    if idd_version in entry.name:
        idd = entry
IDF.setiddname(idd)

# Input JSON file.
# INPUT = args.input_file # original line to be sobstituted with
N_INPUTS = len(args.input_file)
INPUT = args.input_file
#INPUTS = [str(Path(f).resolve()) for f in args.input_file]

# TODO Consider the case in which CSV instead of EPW is given and build EPW from it

# Find and set EPW file.
weather = Path(args.weather).resolve()  # Convert to absolute path.
if weather.is_file():
    EPW = weather
    EPW_DIR = weather.parent
elif weather.is_dir():
    EPW = None
    EPW_DIR = weather
else:
    raise IOError("Error in the EPW weather file")

idf1 = IDF(fname, EPW)
idf1.idfobjects["TIMESTEP"][0]["Number_of_Timesteps_per_Hour"] = 6

DATA_MEAS = str(Path(args.measured_data).resolve())

# Methods for monitored data aggregations handling and KPI DataFrame preparation
def sum_consumption_h(df, building_name):
    
    building_name = building_name.upper()
    df_new = df.copy()
    for col in df_new.columns:
        if "Q_h[kWh/m2]" not in col or building_name not in col.upper():
            df_new = df_new.drop(col, axis=1)

    df["Q_h[kWh/m2]"] = df_new.sum(axis=1,min_count=1)
    # df = df.where(df["Q_h[kWh/m2]"] != np.nan).dropna()
    # print(df)
    return df

def sum_consumption_c(df, building_name):
    
    building_name = building_name.upper()
    df_new = df.copy()
    for col in df_new.columns:
        if "Q_c[kWh/m2]" not in col or building_name not in col.upper():
            df_new = df_new.drop(col, axis=1)

    df["Q_c[kWh/m2]"] = df_new.sum(axis=1)
    return df

def mean_indoor_op_air_temp(df, building_name):
    
    building_name = building_name.upper()
    df_new = df.copy()
    for col in df_new.columns:
        if "T_op_i[C]" not in col or building_name not in col.upper():
            df_new = df_new.drop(col, axis=1)

    df["T_op_i[C]"] = df_new.mean(axis=1)
    return df

def indoor_mean_air_temp(df, building_name):

    building_name = building_name.upper()
    df_new = df.copy()
    for col in df_new.columns:
        if "T_db_i[C]" not in col or building_name not in col.upper():
            df_new = df_new.drop(col, axis=1)

    df["T_db_i[C]"] = df_new.mean(axis=1)
    return df

def indoor_mean_air_RH(df, building_name):

    building_name = building_name.upper()
    df_new = df.copy()
    for col in df_new.columns:
        if "RH_i[%]" not in col or building_name not in col.upper():
            df_new = df_new.drop(col, axis=1)
            
    df["RH_i[%]"] = df_new.mean(axis=1)
    return df

def indoor_mean_rad_temp(df, building_name):

    building_name = building_name.upper()
    df_new = df.copy()
    for col in df_new.columns:
        if "T_rad_i[C]" not in col or building_name not in col.upper():
            df_new = df_new.drop(col, axis=1)
            
    df["T_rad_i[C]"] = df_new.mean(axis=1)
    return df

def indoor_mean_co2(df,building_name):

    building_name = building_name.upper()
    df_new = df.copy()
    for col in df_new.columns:
        if "CO2_i[ppm]" not in col or building_name not in col.upper():
            df_new = df_new.drop(col, axis=1)
    df["CO2_i[ppm]"] = df_new.mean(axis=1)
    return df

def outdoor_mean_co2(df,filter_by = ""):

    df_new = df.copy()
    for col in df_new.columns:
        if "CO2_o[ppm]" not in col or filter_by not in col.upper():
            df_new = df_new.drop(col, axis=1)
            
    df["CO2_o[ppm]"] = df_new.mean(axis=1)
    return df


def detect_decimal(csv_path):
    with open(csv_path, 'r') as f:
        lines = f.readlines()
    point_cnt = np.sum([r.count(".") for r in lines])
    comma_cnt = np.sum([r.count(",") for r in lines])

    if point_cnt > comma_cnt:
        decimal = "."
    elif comma_cnt > point_cnt:
        decimal = ","
    else:
        raise Exception

    return decimal


def main():
    
    # Create empty dataframe for results
    final_df = pd.DataFrame()
    final_timeseries_df = pd.DataFrame()

    # Predyce config
    config = {
        "plot_dir": args.plot_directory,
        "temp_dir": args.temp_directory,
        "out_dir": args.output_directory,
        "num_of_cpus": args.jobs,
        "epw_dir": EPW_DIR
    }

    # Load input file
    with open(INPUT, "r") as f:
        input = json.loads(re.sub("//.*", "", f.read(), flags=re.MULTILINE))
    original_input = input.copy()

    # Run simulations corresponding to different standard profiles
    for cnt,el in enumerate (original_input["preliminary_actions"]):
        #print("--->", cnt, el)#, original_input["preliminary_actions"])

        # modify simulation dates in input file to include warm up
        # set date params --> se cambia il formato con cui sono passati va cambiato questo punto
        start_date = datetime.strptime(original_input["start_date"], "%Y-%m-%d")
        end_date = datetime.strptime(original_input["end_date"], "%Y-%m-%d")

        start_sim = start_date - timedelta(days=7)
        start_sim = start_sim.strftime("%d-%m-%Y")
        end_sim = original_input["end_date"].split("-")[2] +"-"+original_input["end_date"].split("-")[1]+"-"+original_input["end_date"].split("-")[0]
        input["start_date"] = original_input["start_date"].split("-")[2] +"-"+original_input["start_date"].split("-")[1]+"-"+original_input["start_date"].split("-")[0]
        input["end_date"] = end_sim
        # Set preliminary actions (some could not be always necessary, verify with extra demo)

        pre_actions = el

        # set runperiod
        pre_actions["change_runperiod"] = { "start": start_sim,
                                            "end": end_sim,
                                            "fmt": "%d-%m-%Y"}
        # activate co2 computation                                                            
        pre_actions["set_outdoor_co2"] = {"co2_schedule":{
                    "Name": "Default outdoor CO2 levels 400 ppm (Source: NOAA ESRL)",
                    "Schedule_Type_Limits_Name": "Any Number",
                    "Field_1": "Through: 12/31",
                    "Field_2": "For: AllDays",
                    "Field_3": "Until: 24:00",
                    "Field_4": 400
                                                }
                                        }
        # set first day of the year to align week
        day_of_week = start_date.weekday()
        pre_actions["set_first_day"] = {"day": day_of_week+1}

        input["preliminary_actions"] = pre_actions
        # print("\n\n", input, "\n\n")

        idf_run = copy.deepcopy(idf1)
        # Simulation run
        runner = Runner(
            idf_run,
            input,
            **config,
            checkpoint_interval=args.checkpoint_interval,
            graph=True
        )
        runner.create_dataframe(input, args.original)
        runner.run()

        # Read simulation outputs
        filename = Path(args.output_directory) / "data_res.csv"
        res = pd.read_csv(filename, sep=';', index_col=0, parse_dates=True)
        res["data"] = "simulated_" + str(cnt+1)
        for c in res.columns:
            if ":" in c:
                new_c = c.replace(":","_")
                res = res.rename(columns={c:new_c})
        final_df = final_df.append(res,ignore_index=False)

        filename_timeseries = Path(args.plot_directory) / "0/timeseries_data_res.csv"
        res_timeseries = pd.read_csv(filename_timeseries, sep=';', index_col=0, parse_dates=True)
        res_timeseries.columns = ["_".join(("simulated", str(cnt+1), c)) for c in res_timeseries.columns]
        for c in res_timeseries.columns:
            if ":" in c:
                new_c = c.replace(":","_")
                res_timeseries = res_timeseries.rename(columns={c:new_c})
        final_timeseries_df = pd.concat([final_timeseries_df,res_timeseries],axis=1)

    # print(final_df)

    # handle weather data
    weather_df = pd.DataFrame()
    # if start_date.year != end_date.year:          
    weather_df["T_db_o[C]"] = retrieve_column(EPW, "dry_bulb_temperature")
    weather_df["RH_o[%]"] = retrieve_column(EPW, "relative_humidity")
        # TODO check se funziona come controllo che epw con anni multipli non deve essere tmy a caso, non funziona bene questo check
        # previous = weather_df.index[0]
        # for i in weather_df.index:
        #     if weather_df[i] < previous:
        #     # if i.year < start_date or i.year > end_date  or weather_df[i] < previous:
        #         raise Exception("With multiple years run, EPW with real and correct datetimes is needed")
    # dovrebbe andar bene fare così, sovrascritto sia in caso tmy che amy
    # elif start_date.year == end_date.year:
    #     # change simulation year to actual year
    #     final_timeseries_df.index = final_timeseries_df.index.to_series().apply(lambda x: datetime.strptime(datetime.strftime(x, str(start_date.year) + '-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')).tolist()
    #     weather_df["T_db_o[C]"] = retrieve_column(EPW, "dry_bulb_temperature", start_date.year)
    #     weather_df["RH_o[%]"] = retrieve_column(EPW, "relative_humidity", start_date.year)

    # Compute kpi on monitored data and add to final_dataframe
    dateparse = lambda x: datetime.strptime(x, '%Y-%m-%d %H:%M:%S')
    # dateparse = lambda x: datetime.strptime(x, '%d/%m/%Y %H:%M')
    meas_df = pd.read_csv(
        DATA_MEAS,
        sep = ";",
        index_col=0,
        parse_dates=True,
        decimal=detect_decimal(DATA_MEAS),
        date_parser=dateparse
    )

    # Filtra per date
    if start_date is not None:
        meas_df = meas_df[meas_df.index >= start_date]       
        weather_df = weather_df[weather_df.index >= start_date - timedelta(days=7)]
        final_timeseries_df = final_timeseries_df[final_timeseries_df.index >= start_date]

    if end_date is not None:       
        meas_df = meas_df[meas_df.index < end_date]      
        weather_df = weather_df[weather_df.index < end_date]
        final_timeseries_df = final_timeseries_df[final_timeseries_df.index < end_date]

    # TODO capire come gestire meglio e suddividere somme/medie
    meas_df = meas_df.resample("1H").mean()
    
    # per torre pellcie su quali altri sensori è da fare? 
    if "school1_f00_act201ac_MAC0000B319_CO2_i[ppm]" in meas_df.columns:
        meas_df["school1_f00_act201ac_MAC0000B319_CO2_i[ppm]"] += 140

    # TODO fare per tutte le variabili necessarie

    res_df_meas = pd.DataFrame()
    res_df_timeseries = pd.DataFrame()
    res_df_meas["data"] = ["monitored"]
    real_name = input["building_name"]
    try:
        aggregations = input["aggregations"]
    except KeyError:
        aggregations = None
    kpi_list = input["kpi"]
    
    # TODO: Make a separate function that can be used as interpreter always from monitored to kpi
    # TODO: fare per tutte le kpi
    if aggregations is not None:
        for func, agg in aggregations.items():          
            for a in agg:
                a = a.replace(":","_")
                # TODO brutto che queste tre righe vengano eseguite ogni volta
                # temp_df = pd.DataFrame()
                temp_df = weather_df.join(meas_df)
                temp_df["Occupancy"] = 1
                temp_df["heating_season"] = 1
                temp_df["cooling_season"] = 1
                temp_df["free_running_season"] = 1
                temp_df["Date/Time"] = temp_df.index
                temp_df = temp_df.reset_index()
                kpi_instance = KPI(args.plot_directory)
                params = kpi_list[func]
                if "timeseries" not in func:
                    if func == "pmv_ppd":
                        # creare dataframe 
                        temp_df = indoor_mean_air_temp(temp_df,a)
                        temp_df = indoor_mean_rad_temp(temp_df,a)
                        temp_df = indoor_mean_air_RH(temp_df,a)                   
                        temp_df = temp_df.loc[:,["Date/Time", "T_db_i[C]", "RH_i[%]", "T_rad_i[C]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]]
                        # call function
                        kpi = kpi_instance.pmv_ppd(temp_df, **params)
                    elif func == "adaptive_comfort_model":
                        # creare dataframe  
                        temp_df = indoor_mean_air_temp(temp_df,a)
                        temp_df = temp_df.loc[:,["Date/Time","T_db_i[C]","T_db_o[C]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]]
                        # call function          
                        kpi = kpi_instance.adaptive_comfort_model(temp_df,**params)
                    elif func == "co2":
                        # creare dataframe
                        temp_df = indoor_mean_co2(temp_df,a)
                        temp_df = outdoor_mean_co2(temp_df)
                        temp_df = temp_df.loc[:,["Date/Time", "CO2_i[ppm]", "CO2_o[ppm]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                        # call function
                        kpi = kpi_instance.co2(temp_df,**params)
                    elif "n_co2_aIII" in func:
                        # creare dataframe
                        temp_df = indoor_mean_co2(temp_df,a)
                        temp_df = temp_df.loc[:,["Date/Time", "CO2_i[ppm]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                        # call function
                        kpi = kpi_instance.n_co2_aIII(temp_df,**params)
                    elif "n_co2_bI" in func:
                        # creare dataframe
                        temp_df = indoor_mean_co2(temp_df,a)
                        temp_df = temp_df.loc[:,["Date/Time", "CO2_i[ppm]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                        # call function
                        kpi = kpi_instance.n_co2_bI(temp_df,**params) 
                    elif "n_fr" in func:
                        # creare dataframe
                        temp_df = sum_consumption_h(temp_df,a)
                        temp_df = temp_df.loc[:,["Date/Time", "Q_h[kWh/m2]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                        # call function
                        kpi = kpi_instance.n_fr(temp_df,**params) 
                    elif func == "Q_h":
                        # creare dataframe
                        temp_df = sum_consumption_h(temp_df,a)
                        temp_df = temp_df.loc[:,["Date/Time", "Q_h[kWh/m2]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                        # call function
                        kpi = kpi_instance.Q_h(temp_df,**params) 
                    elif func == "Q_c":
                        # creare dataframe
                        temp_df = sum_consumption_c(temp_df,a)
                        temp_df = temp_df.loc[:,["Date/Time", "Q_c[kWh/m2]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                        # call function
                        kpi = kpi_instance.Q_c(temp_df,**params)
                    elif func == "energy_signature":
                        # creare dataframe
                        temp_df = sum_consumption_h(temp_df,a)
                        temp_df = temp_df.loc[:,["Date/Time", "Q_h[kWh/m2]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                        # call function
                        kpi = kpi_instance.Q_h(temp_df,**params)         
                    elif func == "f_Q_h":
                        # creare dataframe
                        temp_df = sum_consumption_h(temp_df,a)
                        temp_df = temp_df.loc[:,["Date/Time", "Q_h[kWh/m2]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                        # call function
                        kpi = kpi_instance.f_Q_h(temp_df,**params) 
                    elif func == "f_Q_c":
                        # creare dataframe
                        temp_df = sum_consumption_c(temp_df,a)
                        temp_df = temp_df.loc[:,["Date/Time", "Q_c[kWh/m2]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                        # call function
                        kpi = kpi_instance.f_Q_c(temp_df,**params)    
                    elif "t_op" in func:
                        # creare dataframe
                        temp_df = indoor_mean_air_temp(temp_df,a)
                        #uguaglianza fittizia di t_db e t_op per i monitorati
                        temp_df = temp_df.rename(columns={"T_db_i[C]": "T_op_i[C]"})
                        temp_df = temp_df.loc[:,["Date/Time", "T_op_i[C]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                        # call function
                        kpi = kpi_instance.t_op(temp_df,**params)                                               
                    else:
                        raise Exception("KPI not found")                       
                    if isinstance(kpi, dict):
                        res_df_meas[func + "_" + a] = [json.dumps(kpi, cls=NpEncoder)]
                    else:
                        res_df_meas[func + "_" + a] = [kpi]
                
                else:
                    if func == "timeseries_co2":
                        # creare dataframe
                        temp_df = indoor_mean_co2(temp_df,a)
                        temp_df = temp_df.loc[:,["Date/Time", "CO2_i[ppm]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]]
                        # call function
                        kpi_timeseries = kpi_instance.timeseries_co2(temp_df,**params)
                    elif "timeseries_n_co2_bI" in func:
                        # creare dataframe
                        temp_df = indoor_mean_co2(temp_df,a)
                        temp_df = temp_df.loc[:,["Date/Time", "CO2_i[ppm]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]]
                        # call function
                        kpi_timeseries = kpi_instance.timeseries_n_co2_bI(temp_df,**params)
                    elif "timeseries_n_co2_aIII" in func:
                        # creare dataframe
                        temp_df = indoor_mean_co2(temp_df,a)
                        temp_df = temp_df.loc[:,["Date/Time", "CO2_i[ppm]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]]
                        # call function
                        kpi_timeseries = kpi_instance.timeseries_n_co2_aIII(temp_df,**params)
                    elif func == "timeseries_acm_hourly_cat":
                        # creare dataframe  
                        temp_df = indoor_mean_air_temp(temp_df,a)
                        temp_df = temp_df.loc[:,["Date/Time","T_db_i[C]","T_db_o[C]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]]
                        # call function          
                        kpi_timeseries = kpi_instance.timeseries_acm_hourly_cat(temp_df,**params)
                    elif func == "timeseries_t_db_i":
                        # creare dataframe
                        temp_df = indoor_mean_air_temp(temp_df,a)
                        temp_df = temp_df.loc[:,["Date/Time", "T_db_i[C]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]]
                        # call function
                        kpi_timeseries = kpi_instance.timeseries_t_db_i(temp_df,**params)
                    elif "timeseries_t_op" in func:
                        # creare dataframe
                        temp_df = indoor_mean_air_temp(temp_df,a)
                        #uguaglianza fittizia di t_db e t_op per i monitorati
                        temp_df = temp_df.rename(columns={"T_db_i[C]": "T_op_i[C]"})
                        temp_df = temp_df.loc[:,["Date/Time", "T_op_i[C]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]]
                        # call function
                        kpi_timeseries = kpi_instance.timeseries_t_op(temp_df,**params)
                    elif "timeseries_n_fr" in func:
                        # creare dataframe
                        temp_df = sum_consumption_h(temp_df,a)
                        temp_df = temp_df.loc[:,["Date/Time", "Q_h[kWh/m2]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                        # call function
                        kpi_timeseries = kpi_instance.timeseries_n_fr(temp_df,**params) 
                    elif func == "timeseries_Q_h":
                        # creare dataframe
                        temp_df = sum_consumption_h(temp_df,a)
                        temp_df = temp_df.loc[:,["Date/Time", "Q_h[kWh/m2]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                        # call function
                        kpi_timeseries = kpi_instance.timeseries_Q_h(temp_df,**params) 
                    elif func == "timeseries_Q_c":
                        # creare dataframe
                        temp_df = sum_consumption_c(temp_df,a)
                        temp_df = temp_df.loc[:,["Date/Time", "Q_c[kWh/m2]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                        # call function
                        kpi_timeseries = kpi_instance.timeseries_Q_c(temp_df,**params)        
                    elif func == "timeseries_f_Q_h":
                        # creare dataframe
                        temp_df = sum_consumption_h(temp_df,a)
                        temp_df = temp_df.loc[:,["Date/Time", "Q_h[kWh/m2]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                        # call function
                        kpi_timeseries = kpi_instance.timeseries_f_Q_h(temp_df,**params) 
                    elif func == "timeseries_f_Q_c":
                        # creare dataframe
                        temp_df = sum_consumption_c(temp_df,a)
                        temp_df = temp_df.loc[:,["Date/Time", "Q_c[kWh/m2]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                        # call function
                        kpi_timeseries = kpi_instance.timeseries_f_Q_c(temp_df,**params)                             

                    # print(func)
                    kpi_timeseries = pd.Series(kpi_timeseries)
                    res_df_timeseries["monitored_" + func + "_" + a] = kpi_timeseries

    kpi_instance = KPI(args.plot_directory)
    for func, params in kpi_list.items():
        temp_df = weather_df.join(meas_df)
        temp_df["Occupancy"] = 1
        temp_df["heating_season"] = 1
        temp_df["cooling_season"] = 1
        temp_df["free_running_season"] = 1
        temp_df["Date/Time"] = temp_df.index
        temp_df = temp_df.reset_index()
        if "timeseries" not in func:
            if func == "pmv_ppd":
                # create dataframe
                temp_df = indoor_mean_air_temp(temp_df,real_name)
                temp_df = indoor_mean_rad_temp(temp_df,real_name)
                temp_df = indoor_mean_air_RH(temp_df,real_name)
                temp_df = temp_df.loc[:,["Date/Time", "T_db_i[C]", "RH_i[%]", "T_rad_i[C]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]]
                # call function
                kpi = kpi_instance.pmv_ppd(temp_df, **params)
            elif func == "adaptive_comfort_model":
                # creare dataframe 
                temp_df = indoor_mean_air_temp(temp_df,real_name)
                temp_df = temp_df.loc[:,["Date/Time","T_db_i[C]","T_db_o[C]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]]
                # call function          
                kpi = kpi_instance.adaptive_comfort_model(temp_df,**params)
            elif "t_op" in func:
                # creare dataframe 
                temp_df = indoor_mean_air_temp(temp_df,real_name)
                #fict associatiaon tdb top per monitorati
                temp_df = temp_df.rename(columns={"T_db_i[C]": "T_op_i[C]"})
                temp_df = temp_df.loc[:,["Date/Time","T_op_i[C]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]]
                # call function          
                kpi = kpi_instance.t_op(temp_df,**params)
            elif func.lower() == "co2":
                # creare dataframe
                temp_df = indoor_mean_co2(temp_df,real_name)
                temp_df = outdoor_mean_co2(temp_df,real_name)
                temp_df = temp_df.loc[:,["Date/Time", "CO2_i[ppm]", "CO2_o[ppm]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                # call function
                kpi = kpi_instance.co2(temp_df,**params)
            elif "n_co2_aIII" in func:
                # creare dataframe
                temp_df = indoor_mean_co2(temp_df,real_name)
                temp_df = temp_df.loc[:,["Date/Time", "CO2_i[ppm]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                # call function
                kpi = kpi_instance.n_co2_aIII(temp_df,**params)
            elif "n_co2_bI" in func:
                # creare dataframe
                temp_df = indoor_mean_co2(temp_df,real_name)
                temp_df = temp_df.loc[:,["Date/Time", "CO2_i[ppm]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                # call function
                kpi = kpi_instance.n_co2_bI(temp_df,**params) 
            elif "n_fr_h" in func:
                # creare dataframe
                temp_df = sum_consumption_h(temp_df,real_name)
                temp_df = temp_df.loc[:,["Date/Time", "Q_h[kWh/m2]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]]
                # call function
                kpi = kpi_instance.n_fr(temp_df,**params)                 
            elif func == "Q_h":
                # creare dataframe
                temp_df = sum_consumption_h(temp_df,real_name)
                temp_df = temp_df.loc[:,["Date/Time", "Q_h[kWh/m2]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]]
                # call function
                kpi = kpi_instance.Q_h(temp_df,**params) 
            elif func == "Q_c":
                # creare dataframe
                temp_df = sum_consumption_c(temp_df,real_name)
                temp_df = temp_df.loc[:,["Date/Time", "Q_c[kWh/m2]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                # call function
                kpi = kpi_instance.Q_c(temp_df,**params)        
            elif func == "f_Q_h":
                # creare dataframe
                temp_df = sum_consumption_h(temp_df,real_name)
                temp_df = temp_df.loc[:,["Date/Time", "Q_h[kWh/m2]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                # call function
                kpi = kpi_instance.f_Q_h(temp_df,**params) 
            elif func == "f_Q_c":
                # creare dataframe
                temp_df = sum_consumption_c(temp_df,real_name)
                temp_df = temp_df.loc[:,["Date/Time", "Q_c[kWh/m2]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                # call function
                kpi = kpi_instance.f_Q_c(temp_df,**params)                  
            else:
                raise Exception("KPI not found: ", func)

            if isinstance(kpi, dict):
                res_df_meas[func] = [json.dumps(kpi, cls=NpEncoder)]
            else:
                res_df_meas[func] = [kpi]
        else:
            if func == "timeseries_co2":
                # creare dataframe
                temp_df = indoor_mean_co2(temp_df,real_name)
                temp_df = temp_df.loc[:,["Date/Time", "CO2_i[ppm]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]]
                # call function
                kpi_timeseries = kpi_instance.timeseries_co2(temp_df,**params)
            elif "timeseries_n_co2_bI" in func:
                # creare dataframe
                temp_df = indoor_mean_co2(temp_df,real_name)
                temp_df = temp_df.loc[:,["Date/Time", "CO2_i[ppm]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]]
                # call function
                kpi_timeseries = kpi_instance.timeseries_n_co2_bI(temp_df,**params)
            elif "timeseries_n_co2_aIII" in func:
                # creare dataframe
                temp_df = indoor_mean_co2(temp_df,real_name)
                temp_df = temp_df.loc[:,["Date/Time", "CO2_i[ppm]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]]
                # call function
                kpi_timeseries = kpi_instance.timeseries_n_co2_aIII(temp_df,**params)
            elif func == "timeseries_t_db_i":
                # creare dataframe
                temp_df = indoor_mean_air_temp(temp_df,real_name)
                temp_df = temp_df.loc[:,["Date/Time", "T_db_i[C]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]]
                # call function
                kpi_timeseries = kpi_instance.timeseries_t_db_i(temp_df,**params)
            elif "timeseries_t_op" in func:
                # creare dataframe
                temp_df = indoor_mean_air_temp(temp_df,real_name)
                temp_df = temp_df.rename(columns={"T_db_i[C]": "T_op_i[C]"})
                temp_df = temp_df.loc[:,["Date/Time", "T_op_i[C]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]]
                # call function
                kpi_timeseries = kpi_instance.timeseries_t_op(temp_df,**params)
            elif func == "timeseries_acm_hourly_cat":
                # creare dataframe
                temp_df = indoor_mean_air_temp(temp_df,real_name)
                temp_df = temp_df.loc[:,["Date/Time","T_db_i[C]","T_db_o[C]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]]
                # call function
                kpi_timeseries = kpi_instance.timeseries_acm_hourly_cat(temp_df,**params)  
            elif "timeseries_n_fr" in func:
                # creare dataframe
                temp_df = sum_consumption_h(temp_df,real_name)
                temp_df = temp_df.loc[:,["Date/Time", "Q_h[kWh/m2]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                # call function
                kpi_timeseries = kpi_instance.timeseries_n_fr(temp_df,**params) 
                # print(kpi_timeseries, "........................")
            elif func == "timeseries_Q_h":
                # creare dataframe
                temp_df = sum_consumption_h(temp_df,real_name)
                temp_df = temp_df.loc[:,["Date/Time", "Q_h[kWh/m2]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                # call function
                kpi_timeseries = kpi_instance.timeseries_Q_h(temp_df,**params) 
            elif func == "timeseries_Q_c":
                # creare dataframe
                temp_df = sum_consumption_c(temp_df,real_name)
                temp_df = temp_df.loc[:,["Date/Time", "Q_c[kWh/m2]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                # call function
                kpi_timeseries = kpi_instance.timeseries_Q_c(temp_df,**params)        
            elif func == "timeseries_f_Q_h":
                # creare dataframe
                temp_df = sum_consumption_h(temp_df,real_name)
                # print(temp_df)
                temp_df = temp_df.loc[:,["Date/Time", "Q_h[kWh/m2]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                # call function
                kpi_timeseries = kpi_instance.timeseries_f_Q_h(temp_df,**params) 
                # print(kpi_timeseries)
            elif func == "timeseries_f_Q_c":
                # creare dataframe
                temp_df = sum_consumption_c(temp_df,real_name)
                temp_df = temp_df.loc[:,["Date/Time", "Q_c[kWh/m2]", "Occupancy", "heating_season", "cooling_season", "free_running_season"]] 
                # call function
                kpi_timeseries = kpi_instance.timeseries_f_Q_c(temp_df,**params)       

            kpi_timeseries = pd.Series(kpi_timeseries)
            res_df_timeseries["monitored_" + func] = kpi_timeseries
            res_df_timeseries = res_df_timeseries[res_df_timeseries.index >= start_date]

    final_df = final_df.append(res_df_meas,ignore_index=False)
    final_df.index = final_df.data
    final_timeseries_df = pd.concat([final_timeseries_df,res_df_timeseries],axis=1)

    final_df.to_csv(Path(args.output_directory) / "final_df.csv", sep=';')
    final_timeseries_df.to_csv(Path(args.output_directory) / "final_timeseries_df.csv", sep=';')

    # Compute delta_kpi simulation vs monitored and add to final dataframe
    cnt = 1
    for i in range (0,len(final_df)):
        if "simulated" in final_df["data"].iloc[i]:
            delta_df = pd.DataFrame()
            delta_df["data"] = ["delta_"+str(cnt)]
            cnt += 1
            for col in final_df.columns:
                if col != "data":
                    if isinstance(final_df[col].iloc[i], str):
                        # print("cella:", final_df[col].iloc[i])
                        sim = dict(json.loads(final_df[col].iloc[i]).items())
                        mon = dict(json.loads(final_df.loc["monitored", col]).items())
                        diff = dict([(k, mon[k] - sim[k]) for k in sim.keys()])
                        diff = json.dumps(diff, cls=NpEncoder)
                        delta_df[col] = [diff]
                    # if isinstance(final_df[col].iloc[i], (float, np.float32, np.float64, int, np.int32, np.int64)):
                    else:
                        diff = final_df.loc["monitored", col] - final_df[col].iloc[i]
                        delta_df[col] = [diff]
    
            final_df = final_df.append(delta_df,ignore_index=False)
    final_df = final_df.reset_index().drop(columns=["index"])
    
    final_df.to_csv(Path(args.output_directory) / "data_res.csv", sep=';')

    # Delta for timeseries KPIs.
    final_timeseries_df_ = final_timeseries_df.copy()
    for c in final_timeseries_df.columns:
        if "simulated" in c:
            for col in final_timeseries_df.columns:
                if "monitored" in col:
                    if col[col.find("timeseries"):] == c[c.find("timeseries"):]:
                        delta = final_timeseries_df[col] - final_timeseries_df[c]
                        final_timeseries_df_["delta" + c[c.find("_"):]] = delta

    final_timeseries_df_.to_csv(Path(args.output_directory) / "data_res_timeseries.csv", sep=';')


if __name__ == "__main__":
    main()
