#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from predyce.IDF_class import IDF
from pathlib import Path
from predyce import idf_editor
from predyce.runner import Runner
from predyce.idf_editor import remove_daylightsaving
import json
import re
import yaml
import numpy as np
import argparse
import tempfile

# Absolute path of the directory of current script.
P = Path(__file__).parent.absolute()

# Configuration file.
with open(P / "config.yml") as config_file:
    CONFIG = yaml.load(config_file, Loader=yaml.FullLoader)

parser = argparse.ArgumentParser()
parser.add_argument("--checkpoint-data", help="Checkpoint data (default: None)", default=None)
parser.add_argument("-c", "--checkpoint-interval", help="Checkpoint interval (default: 128)", default=1024, type=int)
parser.add_argument ("-d", "--output-directory", help="Output directory path (default: /predyce-out)", default=CONFIG["out_dir"])
parser.add_argument("-i", "--idd", help='IDD version', default=CONFIG["idd_version"])
parser.add_argument ("-f", "--input-file", help="Input data file path (default: in.json in executable directory" , default=P / "in.json")
parser.add_argument("-j", "--jobs", help="Multi-process with N processes (0=auto)", default=CONFIG["num_of_cpus"], type=int)
parser.add_argument("-o", "--original", help="Include original model in dataframe permutations (default: True)", action="store_false", default=True)
parser.add_argument("-m", "--measured-data-directory", help="Measured data directory path (default: None)", default=CONFIG["data_true_dir"])
parser.add_argument("-p", "--plot-directory", help="Plot directory path (default: /predyce-plots)", default=CONFIG["plot_dir"])
parser.add_argument("-t", "--temp-directory", help="Temp directory path (default: System temp folder)", default=CONFIG["temp_dir"])
parser.add_argument("-w", "--weather", help="Weather file path (default: "" and must be in case specified in input json file -f)", default="")
parser.add_argument("model", help="Building model path (IDF format only accepted)")

args = parser.parse_args()

# IDF file.
fname1 = args.model

# Find and set IDD file.
idd_folder = Path(__file__).parent.absolute() / "resources" / "iddfiles"
idd_version = str(args.idd).replace(".", "-")
for entry in idd_folder.iterdir():
    if idd_version in entry.name:
        idd = entry
IDF.setiddname(idd)

# Input JSON file.
INPUT = args.input_file

# Find and set EPW file.
weather = Path(args.weather).resolve()  # Convert to absolute path.
if args.weather.endswith(".epw"):
    EPW = weather
    EPW_DIR = weather.parent
else:
    EPW = None
    EPW_DIR = weather
idf1 = IDF(fname1, EPW)

# Set Timestep to 6.
idf1.idfobjects["TIMESTEP"][0]["Number_of_Timesteps_per_Hour"] = 6
remove_daylightsaving(idf1) 

# Checkpoint data.
CHECKPOINT_DATA = args.checkpoint_data

def main():
    # Load input file.

    with open(INPUT, "r") as f:
        input = json.loads(re.sub("//.*", "", f.read(), flags=re.MULTILINE))

    input["actions"] = {}
    for k, v in input["calibration_actions"].items():
        try:
            args_cal = v["args"]
        except Exception:
            args_cal = {}
        if k == "change_ufactor_roofs":
            r = v["range"]
            s = v["step"]
            try_values = np.arange(-r, r, s)
            try_values = list(try_values)
            # AQ double rad
            # try_values = [-0.95]
            # MC double rad
            # try_values = [-0.5]  # saturated
            # SB double rad
            # try_values = [-0.5]  # saturated
            # medie second
            # try_values = [-0.3]
            args_cal = {k: [v] for k, v in args_cal.items()}
            try:
                try_values = v["forced_value"]
            except KeyError:
                pass
            input["actions"][k] = {"ufactor": try_values, **args_cal}

        elif "change_ufactor_windows" in k:
            idf_editor.set_simplified_windows(idf1)
            value = v["value"]
            r = v["range"]
            s = v["step"]
            try_values = np.arange(value-value*r, value+value*r, value*s)
            try_values = list(try_values)
            # SB
            # try_values = [1.3846]
            # SB double rad
            # try_values = [2.3736]
            # AQ
            # try_values = [2.7692]
            # try_values = [2.4725]
            # try_values = [2.5714]
            # MC
            # try_values = [2.4725]
            # medie_second
            # try_values = [1.2857]
            # PG school_1_f01-mod
            # try_values = [1.3846]
            args_cal = {k: [v] for k, v in args_cal.items()}
            try:
                try_values = v["forced_value"]
            except KeyError:
                pass
            input["actions"][k] = {"value": try_values, **args_cal}

        elif k == "change_ufactor_walls" or "change_ufactor" in k:
            r = v["range"]
            s = v["step"]
            try_values = np.arange(-r, r, s)
            try_values = list(try_values)
            # SB
            # try_values = [0.15]
            # MC
            # try_values = [-0.15]
            # AQ
            # try_values = [0]
            # SB double rad
            # try_values = [0.1]
            # medie second
            # try_values = [-0.3]
            args_cal = {k: [v] for k, v in args_cal.items()}
            try:
                try_values = v["forced_value"]
            except KeyError:
                pass
            input["actions"][k] = {"ufactor": try_values, **args_cal}

        if "add_internal_mass" in k:
            # SB
            # try_values = [10.5]
            # AQ
            # try_values = [8.5]
            # MC
            # try_values = [2]
            # SB double rad
            # try_values = [6.5]
            # medie_second
            # try_values = [25]
            # PG school_1_f01-mod
            # try_values = [35]
            args_cal = {k: [v] for k, v in args_cal.items()}
            try:
                try_values = v["forced_value"]
            except KeyError:
                pass
            input["actions"][k] = {"area": try_values, **args_cal}

        if "change_internal_mass" in k:
            try:
                r = v["range"]
                s = v["step"]
            except KeyError:
                pass
            try_values = np.arange(-r, r, s)
            try_values = list(try_values)
            args_cal = {k: [v] for k, v in args_cal.items()}
            try:
                try_values = v["forced_value"]
            except KeyError:
                pass
            input["actions"][k] = {"value": try_values, **args_cal}

        if "change_ach" in k:
            r = v["range"]
            s = v["step"]
            try_values = np.arange(-r, r, s)
            try_values = list(try_values)
            # SB
            # try_values = [-0.5]
            # SB double RAD
            # try_values = [-0.5]
            #MC
            # try_values = [0.15]
            # AQ
            # try_values = [0.25]
            # medie_second
            # try_values = [-0.2]
            # PG school_1_f01-mod
            # try_values = [0] #.9]
            args_cal = {k: [v] for k, v in args_cal.items()}
            try:
                try_values = v["forced_value"]
            except KeyError:
                pass
            input["actions"][k] = {"ach": try_values, **args_cal}

        if "change_infiltration" in k:
            r = v["range"]
            s = v["step"]
            try_values = np.arange(-r, r, s)
            try_values = list(try_values)
            # SB
            # try_values = [0.2]
            # SB double rad
            # try_values = [0.1]
            # MC
            # try_values = [0.1]
            # AQ
            # try_values = [0.25]
            # try_values = [1]
            # medie_second
            # try_values = [0.1]
            # PG school_1_f01-mod
            # try_values = [0.85]
            args_cal = {k: [v] for k, v in args_cal.items()}
            try:
                try_values = v["forced_value"]
            except KeyError:
                pass
            input["actions"][k] = {"ach": try_values, **args_cal}

        if k == "change_internal_heat_gain":
            r = v["range"]
            s = v["step"]
            try_values = np.arange(-r, r, s)
            try_values = list(try_values)
            # SB
            # try_values = [5]
            # MC
            # try_values = [0]
            # AQ
            # try_values = [-0.35]
            # SB double rad
            # Nessun valore, commentare la funzione
            # try_values = [1, 2, 3,]
            # medie_second
            # try_values = [-1]
            # PG school_1_f01-mod
            # try_values = [-0.25]
            args_cal = {k: [v] for k, v in args_cal.items()}
            try:
                try_values = v["forced_value"]
            except KeyError:
                pass
            input["actions"][k] = {"value": try_values, **args_cal}

        if "add_internal_heat_gain" in k:
            args_cal = {k: [v] for k, v in args_cal.items()}
            try:
                try_values = v["forced_value"]
            except KeyError:
                pass
            input["actions"][k] = {"Power per Zone Floor Area": try_values, **args_cal}


        if "change_shgc" in k:
            value = v["value"]
            r = v["range"]
            s = v["step"]
            try_values = np.arange(value-value*r, value+value*r, value*s)
            try_values = list(try_values)
            # SB
            # try_values = [0.8625]
            # SB double rad
            # try_values = [0.3449]
            # AQ
            # try_values = [0.759]  # Old EPW
            # try_values = [0.414]
            # try_values = [0.483]
            # MC
            # try_values = [0.8279]
            # medie_second
            # try_values = [0.69]
            # PG school_1_f01-mod
            # try_values = [0.655]
            args_cal = {k: [v] for k, v in args_cal.items()}
            try:
                try_values = v["forced_value"]
            except KeyError:
                pass
            input["actions"][k] = {"value": try_values, **args_cal}

        if k == "change_visible_transmittance":
            value = v["value"]
            r = v["range"]
            s = v["step"]
            try_values = np.arange(value-value*r, value+value*r, value*s)
            try_values = list(try_values)
            args_cal = {k: [v] for k, v in args_cal.items()}
            try:
                try_values = v["forced_value"]
            except KeyError:
                pass
            input["actions"][k] = {"value": try_values, **args_cal}

        if "change_stack_opening" in k:
            r = v["range"]
            s = v["step"]
            try_values = np.arange(-r, r, s)
            try_values = list(try_values)
            args_cal = {k: [v] for k, v in args_cal.items()}
            try:
                try_values = v["forced_value"]
            except KeyError:
                pass
            input["actions"][k] = {"value": try_values, **args_cal}

        if "add_slat_angle_control" in k:
            r = v["range"]
            s = v["step"]
            try_values = np.arange(-r, r, s)
            try_values = list(try_values)
            args_cal = {k: [v] for k, v in args_cal.items()}
            try:
                try_values = v["forced_value"]
            except KeyError:
                pass
            input["actions"][k] = {"angle": try_values, **args_cal}
        
        if "change_outdoor_air_ach" in k:
            r = v["range"]
            s = v["step"]
            try_values = np.arange(-r, r, s)
            try_values = list(try_values)
            args_cal = {k: [v] for k, v in args_cal.items()}
            try:
                try_values = v["forced_value"]
            except KeyError:
                pass
            input["actions"][k] = {"value": try_values, **args_cal}           


    # Update output variables:
    del idf1.idfobjects["OUTPUT:VARIABLE"][:]
    del idf1.idfobjects["OUTPUT:METER"][:]
    idf_editor.set_outputs(idf1, ["Site Outdoor Air Drybulb Temperature"])
    for obj, rooms in input["calibration_objectives"].items():
        for r in rooms:
            idf_editor.set_outputs(
                idf1,
                variable_list=[obj],
                key_value=r,
                frequency="Timestep",
                reset=False
                )

    # Create Runner and run simulations.
    config = {"plot_dir": args.plot_directory,
              "temp_dir": args.temp_directory,
              "out_dir": args.output_directory,
              "num_of_cpus": args.jobs,
              "epw_dir": EPW_DIR,
              "data_true_dir": Path(args.measured_data_directory) / input["data_true_filename"]
              }
    
    # Plot dir from JSON subfolder.
    try:
        plot_dir_subfolder = input["plot_dir_subfolder"]
        config["plot_dir"] = str(Path(config["plot_dir"]) / plot_dir_subfolder)
    except KeyError:
        pass

    # Out dir from JSON subfolder.
    try:
        out_dir_subfolder = input["out_dir_subfolder"]
        config["out_dir"] = str(Path(config["out_dir"]) / out_dir_subfolder)
    except KeyError:
        pass

    runner = Runner(
        idf1,
        input,
        **config,
        checkpoint_interval=args.checkpoint_interval,
        checkpoint_data=CHECKPOINT_DATA,
        graph=True
    )

    runner.create_dataframe(input, args.original)
    print(runner.df)
    runner.run()
    runner.print_summary()


if __name__ == "__main__":
    main()
